This was created by Ron Williams (http://www.ronsnexus.com/) of Lithic Media (http://www.lithicmedia.com). Please contact me if you are in need of development work. 

To install this module:
Upload to /sites/all/modules 
Dowload http://github.com/stuartloxton/jquery-pushup (find download source if you're not familiar with github)
Extract folder within archive and rename to jquery-pushup. 
Upload file to folder named /sites/all/libraries
Enable on modules page

You're finished! Please contact with any questions. 